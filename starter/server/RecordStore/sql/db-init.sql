-- insert users
-- password is 12345 (bcrypt encoded) 
insert into security_user (username, password, first_name, last_name) values 
	('admin', '$2a$04$4pqDFh9SxLAg/uSH59JCB.LwIS6QoAjM9qcE7H9e2drFuWhvTnDFi', 'Admin', 'Admin');
-- password is abcdef (bcrypt encoded)
insert into security_user (username, password, first_name, last_name) values 
	('petar', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'Petar', 'Petrovic');

-- insert authorities
insert into security_authority (name) values ('ROLE_ADMIN'); -- super user
insert into security_authority (name) values ('ROLE_USER'); -- normal user

-- insert mappings between users and authorities
insert into security_user_authority (user_id, authority_id) values (1, 1); -- admin has ROLE_ADMIN
insert into security_user_authority (user_id, authority_id) values (1, 2); -- admin has ROLE_USER too
insert into security_user_authority (user_id, authority_id) values (2, 2); -- petar has ROLE_USER

insert into category (name) values ("category 1");

insert into project (description, name, category_id, creator_id) values ("description project 1", "project 1", 1, 1);

insert into faq (answer, question, project_id) values ("answer 1", "question 1", 1);
insert into faq (answer, question, project_id) values ("answer 2", "question 2", 1);
insert into faq (answer, question, project_id) values ("answer 3", "question 3", 1);

insert into comment (comment, project_id, user_id) values ("comment 1 by admin", 1, 1);
insert into comment (comment, project_id, user_id) values ("comment 1 by user", 1, 2);

insert into donation (donation, project_id, user_id) values (10, 1, 2);
insert into donation (donation, project_id, user_id) values (200, 1, 2);