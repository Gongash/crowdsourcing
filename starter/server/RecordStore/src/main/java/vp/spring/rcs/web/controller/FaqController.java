package vp.spring.rcs.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Faq;
import vp.spring.rcs.service.FaqService;
import vp.spring.rcs.web.dto.FaqDto;

@RestController
public class FaqController {
	
	@Autowired
	FaqService faqService;
	
	@GetMapping("api/faq")
	public ResponseEntity<List<FaqDto>> findAll(){
		List<FaqDto> dtos = faqService.findAll().stream()
							.map(FaqDto::new)
							.collect(Collectors.toList());
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@PostMapping("/api/faqs")
    public ResponseEntity<FaqDto> create(FaqDto createDto) {
        Faq faq = new Faq();
        faq.setQuestion(createDto.getQuestion());
        faq.setAnswer(createDto.getAnswer());

        faq = faqService.save(faq);

        FaqDto dto = new FaqDto(faq);

        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

}
