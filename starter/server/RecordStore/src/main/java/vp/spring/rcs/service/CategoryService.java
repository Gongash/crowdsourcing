package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.rcs.data.CategoryRepository;
import vp.spring.rcs.model.Category;

@Component
public class CategoryService {
	
	@Autowired
	CategoryRepository categoryRepository;
	
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}
	
	public Category findById(long id) {
		return categoryRepository.findById(id).get();
	}
	
	public Category save(Category category) {
        return categoryRepository.save(category);
    }

}
