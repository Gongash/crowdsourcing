package vp.spring.rcs.web.dto;

import vp.spring.rcs.model.Faq;

public class FaqDto {
	
	private long id;
	
	private String question;
	
	private String answer;
	
	private ProjectDto project;

	public FaqDto() {
		super();
	}
	
	public FaqDto(Faq faq) {
		this.id = faq.getId();
		this.question = faq.getQuestion();
		this.answer = faq.getAnswer();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public ProjectDto getProject() {
		return project;
	}

	public void setProject(ProjectDto project) {
		this.project = project;
	}
	
	

}
