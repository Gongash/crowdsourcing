package vp.spring.rcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Comment;
import vp.spring.rcs.model.user.SecurityUser;
import vp.spring.rcs.service.CommentService;
import vp.spring.rcs.web.dto.CommentDto;

@RestController
public class CommentController {
	
	@Autowired
	CommentService commentService;

	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@GetMapping("api/comment/{id}")
	public ResponseEntity<CommentDto> findById(@PathVariable long id) {
		CommentDto dto = new CommentDto(commentService.findById(id));
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@DeleteMapping("api/comment/{id}")
	public ResponseEntity<Void> deleteById(@PathVariable long id) {
		
		// only the user who made the comment can delete it
		Comment found = commentService.findById(id);
		SecurityUser commenter = found.getUser();
		
		// check current user
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		
		if (username.equalsIgnoreCase(commenter.getUsername())) {
			commentService.deleteById(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
	}
	
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
//	@PostMapping("/api/comments")
//	public ResponseEntity<CommentDto> create (CommentDto commentDto) {
//		Comment comment = new Comment();
//		
//		comment.setUser(commentDto.getUsername());
//		comment.setComment(commentDto.getComment());
//
//        comment = commentService.save(comment);
//
//        CommentDto dto = new CommentDto(comment);
//
//        return new ResponseEntity<>(dto, HttpStatus.CREATED);
//    }
//		
	
	
	
}
