package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.rcs.data.FaqRepository;
import vp.spring.rcs.model.Faq;
import vp.spring.rcs.model.Project;

@Component
public class FaqService {
	
	@Autowired
	FaqRepository faqRepository;
	
	public List<Faq> findAll(){
		return faqRepository.findAll();
	}
	
	public Faq findById(long id) {
		return faqRepository.findById(id).get();
	}

	public Faq save(Faq faq) {
		// TODO Auto-generated method stub
		return faqRepository.save(faq);
	}
	
	

}
