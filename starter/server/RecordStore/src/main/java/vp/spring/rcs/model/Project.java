package vp.spring.rcs.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
@Table(catalog = "crowdsourcing", name = "project")
public class Project {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String name;
	
	private String description;
	
	@OneToMany(mappedBy = "project")
	private Set<Donation> donations = new HashSet<>();
	
	@OneToMany(mappedBy = "project")
	private Set<Faq> faq = new HashSet<>();
	
	@OneToMany(mappedBy = "project")
	private Set<Update> updates = new HashSet<>();
		
	@ManyToOne
	private Category category;
	
	@ManyToOne
	private SecurityUser creator;
	
	private String photoUrl;

	public Project() {
		super();
	}

	public Project(long id, String name, String description, SecurityUser creator) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.creator = creator;
	}

	public Project(long id, String name, String description, Set<Donation> donations, Set<Faq> faq, Set<Update> updates,
			Category category, SecurityUser creator, String photoUrl) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.donations = donations;
		this.faq = faq;
		this.updates = updates;
		this.category = category;
		this.creator = creator;
		this.photoUrl = photoUrl;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Donation> getDonations() {
		return donations;
	}

	public void setDonations(Set<Donation> donations) {
		this.donations = donations;
	}

	public Set<Faq> getFaq() {
		return faq;
	}

	public void setFaq(Set<Faq> faq) {
		this.faq = faq;
	}

	public Set<Update> getUpdates() {
		return updates;
	}

	public void setUpdates(Set<Update> updates) {
		this.updates = updates;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public SecurityUser getCreator() {
		return creator;
	}

	public void setCreator(SecurityUser creator) {
		this.creator = creator;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	
}
