package vp.spring.rcs.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Project;
import vp.spring.rcs.model.user.SecurityUser;
import vp.spring.rcs.service.CategoryService;
import vp.spring.rcs.service.ProjectService;
import vp.spring.rcs.service.UserService;
import vp.spring.rcs.web.dto.ProjectDto;

@RestController
public class ProjectController {
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	UserService userService;
	
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@GetMapping("api/projects")
	public ResponseEntity<List<ProjectDto>> findAll() {
		List<ProjectDto> dtos = projectService.findAll().stream()
								.map(ProjectDto::new)
								.collect(Collectors.toList());
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@GetMapping("api/projects/{id}")
	public ResponseEntity<ProjectDto> findById(@PathVariable long id){
		
		ProjectDto dto = new ProjectDto(projectService.findById(id));
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    @PostMapping("/api/projects")
    public ResponseEntity<Project> create(@RequestBody Project project) {
//        Project project = new Project();
//        project.setName(createDto.getName());
//        project.setDescription(createDto.getDescription());
//        
//        Category category = new Category();
//        category.setId(createDto.getCategory().getId());
//        category.setName(createDto.getCategory().getName());
    	
    	// kratko:
    	// project.setCategory(categoryService.findById(projectDto.getCategoryDto.getId()));
    	
    	//duze:
    	// CategoryDto fromProjectDto = projectDto.getCategory();
    	// Category toNewProject = categoryService.findById(fromProjectDto);
    	// project.setCategory(toNewProject);
    	// project = projectService.save(project);
        
//        project.setCategory(category);
        SecurityUser creator = userService.findById(1);
        project.setCreator(creator);

        project = projectService.save(project);
        project.setCreator(null);
//        ProjectDto dto = new ProjectDto(project);
        return new ResponseEntity<>(project, HttpStatus.CREATED);
    }
    
//    @RequestMapping(value="api/projects/search", method = RequestMethod.GET) 
//	public String getProjectsByName(@RequestParam String name) {
//		List<Project> projects = projectService.findByName(name);
//		
//		StringBuffer sb = new StringBuffer();
//		for (Project p : projects) {
//			sb.append(p).append("\n"); 
//		}
//		
//		return sb.toString();
//	}

}
