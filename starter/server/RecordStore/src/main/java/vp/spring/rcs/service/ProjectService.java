package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.spring.rcs.data.ProjectRepository;
import vp.spring.rcs.model.Project;

@Component
public class ProjectService {
	
	@Autowired
	ProjectRepository projectRepository;
	
	public List<Project> findAll() {
		return projectRepository.findAll();
	}
	
	public Page<Project> findAllPage(Pageable page) {
		return projectRepository.findAll(page);
	}
	
	public Project findById(long id) {
		return projectRepository.findById(id).get();
	}

	public Project save(Project project) {
		return projectRepository.save(project);
	}

//	public List<Project> findByName(String name) {
//		return ProjectRepository.findByNameContains(name);
//	}

}
