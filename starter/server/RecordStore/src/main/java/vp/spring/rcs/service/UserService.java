package vp.spring.rcs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.rcs.data.UserRepository;
import vp.spring.rcs.model.user.SecurityUser;

@Component
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	public SecurityUser findById(long id) {
		return userRepository.getOne(id);
	}

}
