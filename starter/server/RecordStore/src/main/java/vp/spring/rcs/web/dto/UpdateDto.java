package vp.spring.rcs.web.dto;

import vp.spring.rcs.model.Update;

public class UpdateDto {
	
	private long id;
	
	private String title;
	
	private String text;
	
//	private ProjectDto project;

	private long projectId;
	
	public UpdateDto() {
		super();
	}
	
	public UpdateDto(Update update) {
		this.id = update.getId();
		this.title = update.getTitle();
		this.text = update.getText();
		this.projectId = update.getProject().getId();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

//	public ProjectDto getProject() {
//		return project;
//	}
//
//	public void setProject(ProjectDto project) {
//		this.project = project;
//	}

}
