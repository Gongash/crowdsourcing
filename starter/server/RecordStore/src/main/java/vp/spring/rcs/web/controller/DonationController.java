package vp.spring.rcs.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.service.DonationService;
import vp.spring.rcs.web.dto.CategoryDto;
import vp.spring.rcs.web.dto.DonationDto;

@RestController
public class DonationController {

	@Autowired
	DonationService donationService;
	
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@GetMapping("api/donations")
	public ResponseEntity<List<DonationDto>> findAll() {
		List<DonationDto> dtos = donationService.findAll().stream()
				.map(DonationDto::new)
				.collect(Collectors.toList());
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);

	}
	
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@GetMapping("api/donations/{id}")
	public ResponseEntity<DonationDto> findById(@PathVariable long id) {
		
		DonationDto dto = new DonationDto(donationService.findById(id));
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
		
	}
	
	
	
	
}
