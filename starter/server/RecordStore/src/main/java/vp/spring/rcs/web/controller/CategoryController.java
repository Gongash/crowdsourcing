package vp.spring.rcs.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Category;
import vp.spring.rcs.service.CategoryService;
import vp.spring.rcs.service.ProjectService;
import vp.spring.rcs.web.dto.CategoryDto;
import vp.spring.rcs.web.dto.ProjectDto;

@RestController
public class CategoryController {

	@Autowired
	CategoryService categoryService;
	
	@Autowired
	ProjectService projectService;
	
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@GetMapping("api/categories")
	public ResponseEntity<List<CategoryDto>> findAll(){
		List<CategoryDto> dtos = categoryService.findAll().stream()
								.map(CategoryDto::new)
								.collect(Collectors.toList());
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@GetMapping("api/categories/{id}")
	public ResponseEntity<CategoryDto> findById(@PathVariable long id) {
		
		CategoryDto dto = new CategoryDto(categoryService.findById(id));
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	
	@GetMapping("api/categories/{id}/projects")
	public ResponseEntity<List<ProjectDto>> getProjectsByCategory(@PathVariable long id) {
		List<ProjectDto> dtos = projectService.findAll().stream()
								.filter(project -> project.getCategory().getId() == id)
								.map(ProjectDto::new)
								.collect(Collectors.toList());
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    @PostMapping("/api/categories")
    public ResponseEntity<Category> createCategory(@RequestBody Category category) {

		category = categoryService.save(category);

        return new ResponseEntity<>(category, HttpStatus.CREATED);
    }
	
}
