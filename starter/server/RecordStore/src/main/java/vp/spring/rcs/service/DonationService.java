package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.rcs.data.DonationRepository;
import vp.spring.rcs.model.Donation;

@Component
public class DonationService {

	@Autowired
	DonationRepository donationRepository;
	
	public List<Donation> findAll(){
		return donationRepository.findAll();
	}
	
	public Donation findById(long id) {
		return donationRepository.findById(id).get();
	}
}

