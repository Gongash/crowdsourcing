package vp.spring.rcs.web.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import vp.spring.rcs.model.Project;

public class ProjectDto {
	
	private long id;
	
	private String name;
	
	private String description;
	
	private List<DonationDto> donations = new ArrayList<>();
	
	private List<FaqDto> faqs = new ArrayList<>();
	
	private List<UpdateDto> updates = new ArrayList<>();
	
	private CategoryDto category;
	
	private String creatorUsername;
	
	private String photoUrl;

	public ProjectDto() {
		super();
	}
	
	public ProjectDto(Project project) {
		this.id = project.getId();
		this.name = project.getName();
		this.description = project.getDescription();
		this.category = new CategoryDto(project.getCategory());
		this.creatorUsername = project.getCreator().getUsername();
		this.faqs = project.getFaq().stream()
					.map(FaqDto::new)
					.collect(Collectors.toList());
		this.updates = project.getUpdates().stream()
						.map(UpdateDto::new)
						.collect(Collectors.toList());
		this.donations = project.getDonations().stream()
						.map(DonationDto::new)
						.collect(Collectors.toList());
		this.photoUrl = project.getPhotoUrl();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DonationDto> getDonations() {
		return donations;
	}

	public void setDonations(List<DonationDto> donations) {
		this.donations = donations;
	}

	public List<FaqDto> getFaqs() {
		return faqs;
	}

	public void setFaqs(List<FaqDto> faqs) {
		this.faqs = faqs;
	}

	public List<UpdateDto> getUpdates() {
		return updates;
	}

	public void setUpdates(List<UpdateDto> updates) {
		this.updates = updates;
	}

	public CategoryDto getCategory() {
		return category;
	}

	public void setCategory(CategoryDto category) {
		this.category = category;
	}

	public String getCreatorUsername() {
		return creatorUsername;
	}

	public void setCreatorUsername(String creatorUsername) {
		this.creatorUsername = creatorUsername;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

}
