package vp.spring.rcs.web.dto;

import java.util.ArrayList;
import java.util.List;

import vp.spring.rcs.model.Category;

public class CategoryDto {
	
	private long id;
	
	private String name;
	
//	private List<ProjectDto> projects = new ArrayList<>();

	public CategoryDto() {
		super();
	}

	public CategoryDto(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public CategoryDto(Category category) {
		this.id = category.getId();
		this.name = category.getName();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public List<ProjectDto> getProjects() {
//		return projects;
//	}
//
//	public void setProjects(List<ProjectDto> projects) {
//		this.projects = projects;
//	}

}
