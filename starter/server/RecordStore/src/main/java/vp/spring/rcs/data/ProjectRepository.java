package vp.spring.rcs.data;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.spring.rcs.model.Project;

@Component
public interface ProjectRepository extends JpaRepository<Project, Long> {

//	public static List<Project> findByNameContains(String name) {
//		List<Project> retVal = new ArrayList<Project>();
//
//		for (Project project: projects) {
//			if (project.getName().contains(name)) {
//				retVal.add(project);
//			}
//		}
//		return retVal;
//	}
	
	public Project getProjectByName(String name);

}
