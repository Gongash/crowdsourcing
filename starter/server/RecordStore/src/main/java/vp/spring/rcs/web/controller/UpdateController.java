package vp.spring.rcs.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Project;
import vp.spring.rcs.model.Update;
import vp.spring.rcs.service.ProjectService;
import vp.spring.rcs.service.UpdateService;
import vp.spring.rcs.web.dto.UpdateDto;

@RestController
public class UpdateController {

	@Autowired
	UpdateService updateService;
	
	@Autowired
	ProjectService projectService;

	// @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@GetMapping("api/updates")
	public ResponseEntity<List<UpdateDto>> findAll() {
		List<UpdateDto> dtos = updateService.findAll().stream().map(UpdateDto::new).collect(Collectors.toList());

		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

	// @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@PostMapping("/api/updates")
	public ResponseEntity<UpdateDto> create(@RequestBody UpdateDto createDto) {
		Update update = new Update();
		update.setTitle(createDto.getTitle());
		update.setText(createDto.getText());
		
		Project project = projectService.findById(createDto.getProjectId());
		update.setProject(project);

		update = updateService.save(update);
		UpdateDto dto = new UpdateDto(update);

		return new ResponseEntity<>(dto, HttpStatus.CREATED);
	}

}
