package vp.spring.rcs.web.dto;

import vp.spring.rcs.model.Comment;

public class CommentDto {
	
	private long id;
	
	private String username;
	
	private String comment;
	
	private ProjectDto project;

	public CommentDto() {
		super();
	}
	
	public CommentDto(Comment comment) {
		this.id = comment.getId();
		this.username = comment.getUser().getUsername();
		this.comment = comment.getComment();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public ProjectDto getProject() {
		return project;
	}

	public void setProject(ProjectDto project) {
		this.project = project;
	}
	
	

}
