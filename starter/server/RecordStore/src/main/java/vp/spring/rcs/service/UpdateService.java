package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.rcs.data.UpdateRepository;
import vp.spring.rcs.model.Update;

@Component
public class UpdateService {
	
	@Autowired
	UpdateRepository updateRepository;
	
	public List<Update> findAll(){
		return updateRepository.findAll();
	}
	
	public Update findById(long id) {
		return updateRepository.findById(id).get();
	}

	public Update save(Update update) {
		return updateRepository.save(update);
	}

}
