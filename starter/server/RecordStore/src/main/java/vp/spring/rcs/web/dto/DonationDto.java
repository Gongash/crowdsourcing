package vp.spring.rcs.web.dto;

import vp.spring.rcs.model.Donation;

public class DonationDto {
	
	private long id;
	
	private double donation;
	
	private String donatorUsername;
	
	private ProjectDto project;

	public DonationDto() {
		super();
	}
	
	public DonationDto(Donation donation) {
		this.id = donation.getId();
		this.donation = donation.getDonation();
		this.donatorUsername = donation.getUser().getUsername();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getDonation() {
		return donation;
	}

	public void setDonation(double donation) {
		this.donation = donation;
	}

	public String getDonatorUsername() {
		return donatorUsername;
	}

	public void setDonatorUsername(String donatorUsername) {
		this.donatorUsername = donatorUsername;
	}

	public ProjectDto getProject() {
		return project;
	}

	public void setProject(ProjectDto project) {
		this.project = project;
	}
	
	

}
