import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

// import { PROJECT_LIST } from './mock-project-list';
import { Project } from './project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http: HttpClient) { }

  save(project: Project): Observable<any> {
    console.log('Trying to save project', project);
    // call remote server
    return this.http.post('http://localhost:8080/api/projects', project);
  }

  // getProjectList(): Observable<Project[]> {
  //   return of (PROJECT_LIST);
  // }

  getProjectList(): Observable<any> {
    return this.http.get('http://localhost:8080/api/projects');
  }

  getProjectByCategory(categoryId): Observable<any> {
    console.log('usao i pozvao sa id', categoryId);
    return this.http.get('http://localhost:8080/api/categories/' + categoryId + '/projects');
  }
}
