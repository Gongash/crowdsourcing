import { Component, OnInit } from '@angular/core';
import { Project } from '../project';

import { ProjectService } from '../project.service';
import { CategoryService } from '../category.service';
import { Category } from '../category';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {

  project: Project = {
    id: null,
    name: '',
    description: '',
    category: null,
    photoUrl: ''
  };

  categories: Category[] = null;

  constructor(private projectService: ProjectService, private categoryService: CategoryService) { }

  ngOnInit() {
    this.categoryService.getAllCategories().subscribe(result => this.categories = result);
  }

  submitProject(project: Project) : void {
    // call service to save the project
    this.projectService.save(project).subscribe(result => console.log('result', result));
  }

  setCategory(category: Category) : void {
    console.log('category', category);
    this.project.category = category;
  }

}
