import { Category } from './category';

export class Project {
  id: number;
  name: string;
  description: string;
  category: Category;
  photoUrl: string;
}