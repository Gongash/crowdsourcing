import { Component, OnInit } from '@angular/core';
import { Category } from '../category';

import { CategoryService } from '../category.service';

@Component({
  selector: 'app-category-create',
  templateUrl: './category-create.component.html',
  styleUrls: ['./category-create.component.css']
})
export class CategoryCreateComponent implements OnInit {

  category: Category = {
    id: null,
    name: ''
  }

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
  }

  submitCategory(category: Category) : void {
    console.log('category', category);
    this.categoryService.save(category).subscribe(result => console.log('result', result));
  }

}
