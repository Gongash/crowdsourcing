import { Component, OnInit } from '@angular/core';
import { Project } from '../project';
import { ProjectService } from '../project.service';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  projectList: Project[];

  constructor(private projectService: ProjectService, private route: ActivatedRoute) { }

  ngOnInit() {
    // const categoryId = +this.route.snapshot.paramMap.get('id');
    // this.projectService.getProjectByCategory(categoryId).subscribe(result => {
    //   this.projectList = result;
    // });

    this.route.paramMap.subscribe(params => {
      const categoryId = +params.get('id');
      this.projectService.getProjectByCategory(categoryId).subscribe(result => {
        this.projectList = result;
      });
    });
  }

}
