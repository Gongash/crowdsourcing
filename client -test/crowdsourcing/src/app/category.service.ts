import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';


import { CATEGORY_LIST } from './mock-category-list';
import { Category } from './category';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private categoriesUrl = 'http://localhost:8080/api/categories';
  // private res: Response;

  constructor(
    private http: HttpClient) {
      // this.loadData();
    }

  save(category: Category): Observable<any> {
    console.log('Trying to save category', category);
    // call remote server
    return this.http.post(this.categoriesUrl, category);
  }

  getAllCategories(): Observable<any> {
    return this.http.get(this.categoriesUrl);
  }

  // loadData() {
  //   this.http.get('/api/categories').subscribe((cat: Category[]) => this.data(cat));
  // }
/** POST: add a new category to the server */
  // addCategory(category: Category) {
  //   let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  //   this.http.post('api/categories', JSON.stringify(category), { headers }).subscribe(() => {
  //     this.loadData();
  //   });
  // }

}


