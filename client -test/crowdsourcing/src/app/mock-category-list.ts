import { Category } from './category';

export const CATEGORY_LIST: Category[] = [
  { id: 1, name: 'Games'},
  { id: 2, name: 'Music'},
  { id: 3, name: 'Movies'},
  { id: 1, name: 'Food'}
];